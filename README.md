# Automate Docker using Ansible Part 1

#### Project Outline

This project involves setting up AWS infrastructure and writing an Ansible Playbook to automate the installation of Docker and Docker-compose on an AWS EC2 instance. The process includes configuring the Ansible environment on an EC2 instance running Amazon Linux, connecting to the server via Ansible, setting up an inventory file for EC2 connection, installing Docker and Docker-compose, copying the docker-compose file to the server, and finally, starting the Docker containers to run the application.

#### Lets get started

Using the terraform file from the below link

[Terraform Automate AWS Infrastructure](https://gitlab.com/FM1995/terraform-automate-aws-infrastructure.git)


Can use the below commands

```
terraform init, terraform apply
```

Can see it has completed

![Image 1](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image1.png)

And the ec2 instance is now running live with no configuration yet

![Image 2](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image2.png)

Can then modify the hosts file which will use the ec2 user and the public key to connect to connect to the ansible server

![Image 3](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image3.png)

Now we need to see which package manager is needed in our case it will be yum

![Image 4](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image4.png)

So will need to use yum

Now using the below package manager docs

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html

Can utilise the below

![Image 5](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image5.png)

Can then proceed to create the playbook

![Image 6](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image6.png)

Now we need to tell Ansible to configure the above as a root user, and to switch to the root user we can use the below documentation

https://docs.ansible.com/ansible/latest/collections/community/general/sudosu_become.html

![Image 7](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image7.png)

Can then execute the below command

```
ansible-playbook -I hosts deploy-docker.yaml
```

![Image 8](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image8.png)

From the previous screenshots can see the python version 2 was installed when it should have been 3 hence the warning

Can see docker is installed in the ec2 instance

![Image 9](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image9.png)

Now lets implement logic so that python3 is installed and therefore the docker installation will use python3 instead of 2

![Image 10](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image10.png)

And now run the playbook

```
ansible-playbook -I hosts deploy-docker.yaml
```

Can see python3 is now installed

![Image 11](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image11.png)

Going in to the ec2 server can see python3

![Image 12](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image12.png)

However we are not using it as we are still getting the warning when we execute the playbook. We can resolve that in the ansible.cfg file just like the below

![Image 13](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image13.png)

Lets say we have a fresh server and the interpreter is not there yet with python3 yet

So what we can do is tell ansible to install ansible using the default python2 installtion, like the below

![Image 14](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image14.png)

And then add the interpreter on a task level to use pyhton2 to install python3 

![Image 15](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image15.png)

And we also need to install docker using python2 using yum

![Image 16](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image16.png)

So in essence we are telling ansible to use pyhton3 but for specific tasks, we are telling ansible to use pyhton2 to install packages

And because they python3 and docker use python2 we can make it into one play

![Image 17](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image17.png)

Again executing

```
ansible-playbook -I hosts deploy-docker.yaml
```

Can see the below results

![Image 18](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image18.png)

Now lets install docker compose in the plays

Since docker-compose isn’t available locally

![Image 19](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image19.png)

Will also include it in the playbook similar to a project we did earlier

Using the entry script from the previous project to install docker compose

https://gitlab.com/FM1995/cicd-with-terraform-aws.git

![Image 20](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image20.png)

Can then utilise the get url function to install docker compose

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html

Can then create another play utilising the below

![Image 21](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image21.png)

![Image 22](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image22.png)

Can then store the download in a location of the below

![Image 23](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image23.png)

Can then set the mode and make it executable

![Image 24](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image24.png)

Upon checking, can see the uname gives the below

![Image 26](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image26.png)

Can hard code it in the playbook

This part of the command will be interpreted as a string and not as a command

![Image 27](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image27.png)

To fix it

Can hardcode it like the below using the above

![Image 28](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image28.png)

For uname -m can hardcode it, however can use 

Can use lookup/ pipe lookup to allow ansible to access data from outside source and it then calculates the output of the shell command and pipes it to the left side of the lookup

Can use the below documentation

https://docs.ansible.com/ansible/latest/plugins/lookup.html

![Image 29](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image29.png)

Can then configure it like the below from the documentation

![Image 30](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image30.png)

Can then configure it like the below

![Image 31](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image31.png)

Progressed play now for docker compose to execute as a root user

![Image 32](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image32.png)

Now ready to run the playbook

![Image 33](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image33.png)

Can then run docker-compose in the ec2 server

![Image 34](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image34.png)

Next we will start docker daemon

![Image 35](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image35.png)

Using the below link

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_service_module.html

![Image 36](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image36.png)

Can then configure the below

![Image 37](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image37.png)

Can then execute the playbook again

![Image 38](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image38.png)

Now we need to add the ec2-user to the docker group

Can then use the below link to configure the user to be added to the group

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html

Progressed playbook

![Image 39](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image39.png)

Can then also include a test

![Image 40](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image40.png)

So an interesting case, is when the ec2-user is added to the docker group, it doesn’t get added instantly and only gets re-added on a re-connection, when logging out and then logging back in

Can use the below module

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/meta_module.html

![Image 41](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image41.png)

So as soon as we add the user to the group, we can reset the connection, thus establishing the ec2 user is added to the docker group and execute docker commands

Once done can pull redis image using docker

![Image 42](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image42.png)

Can see it was able to execute docker commands

![Image 43](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image43.png)

Can then check the docker images

![Image 44](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1/-/raw/main/Images/Image44.png)















